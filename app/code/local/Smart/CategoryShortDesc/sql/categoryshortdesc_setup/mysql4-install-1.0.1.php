<?php
$installer = $this;
$installer->startSetup();
$attribute  = array(
    'type'          =>  'varchar',
    'label'         =>  'Category Short Description',
    'input'         =>  'textarea',
    'backend'       =>  '',
    'global'        =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'       =>  true,
    'required'      =>  false,
    'user_defined'  =>  true,
    'group'         =>  "General Information"
);
$installer->addAttribute('catalog_category', 'categoryshortdesc', $attribute);
$installer->endSetup();
?>